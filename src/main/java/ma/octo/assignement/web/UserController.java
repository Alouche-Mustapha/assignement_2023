package ma.octo.assignement.web;

import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.Utilisateur;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "users")
public class UserController {
    private final CompteRepository compteRepository;
    private final UtilisateurRepository utilisateurRepository;

    Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    public UserController(CompteRepository accountRepository, UtilisateurRepository userRepository) {
        this.compteRepository = accountRepository;
        this.utilisateurRepository = userRepository;
    }

    @GetMapping("listOfAccounts")
    List<Compte> loadAllCompte() {
        LOGGER.info("Liste des comptes");
        List<Compte> all = compteRepository.findAll();
        return CollectionUtils.isEmpty(all) ? null : all;
    }

    @GetMapping("lister_utilisateurs")
    List<Utilisateur> loadAllUtilisateur() {
        LOGGER.info("Liste des utilisateurs");
        List<Utilisateur> all = utilisateurRepository.findAll();
        return CollectionUtils.isEmpty(all) ? null : all;
    }

}
