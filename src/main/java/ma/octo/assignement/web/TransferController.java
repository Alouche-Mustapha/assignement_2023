package ma.octo.assignement.web;

import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(path = "/transfers")
class TransferController {

    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    private final CompteRepository compteRepository;
    private final TransferRepository transferRepository;
    private final AuditService auditService;
    private final TransferService transferService;

    public TransferController(CompteRepository compteRepository, TransferRepository transferRepository, AuditService auditService, TransferService transferService) {
        this.compteRepository = compteRepository;
        this.transferRepository = transferRepository;
        this.auditService = auditService;
        this.transferService = transferService;
    }

    @GetMapping("listDesTransferts")
    List<Transfer> loadAll() {
        LOGGER.info("Liste des utilisateurs");
        var all = transferRepository.findAll();
        return CollectionUtils.isEmpty(all) ? all : null;
    }

    @PostMapping("/executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        Compte compteEmetteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        transferService.checkValidity(compteEmetteur, compteBeneficiaire, transferDto, MONTANT_MAXIMAL);
        transferService.makeTransfer(compteEmetteur, compteBeneficiaire, transferDto);

        auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                        .toString());
    }
}
