package ma.octo.assignement.web;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.DepositService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/deposit")
public class DepositController {

    Logger LOGGER = LoggerFactory.getLogger(DepositController.class);

    public static final long MONTANT_MAXIMAL = 10000;

    private final CompteRepository accountRepository;
    private final DepositService depositService;
    private final AuditService auditService;

    DepositController(CompteRepository accountRepository, DepositService depositService, AuditService auditService) {
        this.accountRepository = accountRepository;
        this.depositService = depositService;
        this.auditService = auditService;
    }

    @PostMapping("/executerDeposit")
    @ResponseStatus(HttpStatus.CREATED)
    public void createDeposit(@RequestBody DepositDto depositDto) throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        LOGGER.info("Creation de depos");
        Compte account = accountRepository.findByRib(depositDto.getRib());

        depositService.checkValidity(depositDto.getMontant(), MONTANT_MAXIMAL);
        depositService.makeDeposit(account, depositDto);
        auditService.auditDeposit(
                "Deposit au compte " +
                        depositDto.getRib() +
                        " d'un montant de " +
                        depositDto.getMontant().toString()
        );
    }
}
