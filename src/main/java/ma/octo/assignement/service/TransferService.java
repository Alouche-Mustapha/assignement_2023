package ma.octo.assignement.service;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.Transfer;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
@Transactional
public class TransferService {

    Logger LOGGER = LoggerFactory.getLogger(TransferService.class);

    private final TransferRepository transferRepository;
    private final CompteRepository accountRepository;

    public TransferService(TransferRepository transferRepository, CompteRepository accountRepository) {
        this.transferRepository = transferRepository;
        this.accountRepository = accountRepository;
    }

    public void makeTransfer(Compte compteEmetteur, Compte compteBeneficiaire, TransferDto transferDto) {
        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(transferDto.getMontant()));
        accountRepository.save(compteEmetteur);

        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().intValue() + transferDto.getMontant().intValue()));
        accountRepository.save(compteBeneficiaire);

        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setCompteBeneficiaire(compteBeneficiaire);
        transfer.setCompteEmetteur(compteEmetteur);
        transfer.setMontantTransfer(transferDto.getMontant());

        transferRepository.save(transfer);
    }

    public void checkValidity(Compte compteEmetteur, Compte compteBeneficiaire, TransferDto transferDto, int MONTANT_MAXIMAL) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        if (compteEmetteur == null || compteBeneficiaire == null) {
            LOGGER.error("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        } else if (transferDto.getMontant().intValue() == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (transferDto.getMontant().intValue() < 10) {
            LOGGER.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        } else if (compteEmetteur.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }
    }

}
