package ma.octo.assignement.service;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.MoneyDeposit;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;

@Service
@Transactional
public class DepositService {
    Logger LOGGER = LoggerFactory.getLogger(DepositService.class);

    private final CompteRepository accountRepository;
    private final MoneyDepositRepository moneyDepositRepository;

    public DepositService(CompteRepository accountRepository, MoneyDepositRepository moneyDepositRepository) {
        this.accountRepository = accountRepository;
        this.moneyDepositRepository = moneyDepositRepository;
    }

    public void checkValidity(BigDecimal montant, long MONTANT_MAXIMAL) throws TransactionException {
        if (montant == null || montant.intValue() == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        }
        if (montant.intValue() < 10) {
            LOGGER.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        }
        if (montant.intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }
    }

    public void makeDeposit(Compte account, DepositDto depositDto) {
        account.setSolde(account.getSolde().add(depositDto.getMontant()));
        accountRepository.save(account);

        MoneyDeposit moneyDeposit = new MoneyDeposit();
        moneyDeposit.setMontant(depositDto.getMontant());
        moneyDeposit.setMotifDeposit(depositDto.getMotif());
        moneyDeposit.setCompteBeneficiaire(account);
        moneyDeposit.setDateExecution(new Date());
        moneyDeposit.setNomPrenomEmetteur(depositDto.getNomPrenomEmetteur());

        moneyDepositRepository.save(moneyDeposit);
    }
}
